const mix = require('laravel-mix');

mix
    .js('src/vue-accordion.js', 'dist')
    .setPublicPath('dist');
