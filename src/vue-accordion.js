import Accordion from './js/Accordion.vue';
import AccordionEl from './js/AccordionEl.vue';

/**
 * Vue Accordion Plugin
 * @example
 * Vue.use(VueAccordion);
 */
export default {
    name: 'vue-accordion',
    production: process.env.NODE_ENV === 'production',
    install(Vue) {
        Object.entries({
            'cp-accordion': Accordion,
            'cp-accordion-el': AccordionEl,
        }).forEach(([name, Component]) => Vue.component(name, Component));
    },
};
