# VueAccordion

...


## Installation

`npm install git+ssh://git@gitlab.com:cherrypulp/components/vue-accordion.git`


## Quick start

### Initialization

```javascript
import VueAccordion from '@cherrypulp/vue-accordion';

Vue.use(VueAccordion);
```

then in templates

```html
<cp-accordion :toggle="true">
  <cp-accordion-el title="Title" content="This is a test" :open="true">
  </cp-accordion-el>
</cp-accordion>
```

or 

```html
<cp-accordion :toggle="true">
  <cp-accordion-el>
    <template v-slot:title>Title 2</template>
    <template v-slot:content>This is a test 2</template>
    <template v-slot:arrow>></template>
  </cp-accordion-el>
</cp-accordion>
```

The title and content are required.
